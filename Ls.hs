module Main where

import System.Directory
import System.Environment
import System.Console.ANSI

joinWithNewline :: [String] -> String
joinWithNewline [] = ""
joinWithNewline [x] = x
joinWithNewline (x:xs) = x ++ "\n" ++ joinWithNewline xs

formatDirs :: [(FilePath, [FilePath])] -> [String]
formatDirs [] = []
formatDirs [(dir, contents)] = [setSGRCode [SetColor Foreground Dull Red]
                                ++ dir ++ "\n\n"
                                ++ setSGRCode [Reset]
                                ++ joinWithNewline contents]
formatDirs (x:xs) = (formatDirs [x]) ++ [""] ++ (formatDirs xs)

main :: IO ()
main = do
    args <- getArgs
    case args of
      [] -> do
          contents <- getCurrentDirectory >>= getDirectoryContents
          mapM_ putStrLn contents
      _ -> do
          contents <- mapM getDirectoryContents args
          mapM_ putStrLn (formatDirs $ zip args contents)
