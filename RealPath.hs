module Main where

import System.Directory
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    mapM_ (\f -> makeAbsolute f >>= canonicalizePath >>= putStrLn) args
