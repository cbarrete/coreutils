module Main where

import System.FilePath
import System.Directory
import System.Environment
import System.IO

process :: (String, String) -> FilePath
process (dir, "") = dir
process (_, file) = file

main :: IO ()
main = do
    args <- getArgs
    case args of
      [] -> hPutStrLn stderr "No filename given"
      _  -> putStrLn . process . splitFileName $ head args
