module Main where

import Control.Monad
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    case length args of
      0 -> forever (putStrLn "y")
      _ -> forever $ putStrLn $ unwords args
