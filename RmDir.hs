module Main where

import System.Directory
import System.Environment
import System.IO

main :: IO ()
main = do
    args <- getArgs

    let func = if elem "-r" args
                  then removeDirectoryRecursive
                  else removeDirectory

    let cleaned = filter (/= "-r") args

    case args of
      [] -> hPutStrLn stderr "No directory given"
      _ -> mapM_ func cleaned
