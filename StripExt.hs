module Main where

import System.Environment
import System.FilePath
import System.IO

process :: [String] -> [String]
process ("--one":filenames) = map dropExtension filenames
process ("-o":filenames)    = map dropExtension filenames
process ("--all":filenames) = map dropExtensions filenames
process ("-a":filenames)    = map dropExtensions filenames
process filenames           = map dropExtensions filenames

main :: IO ()
main = do
    args <- getArgs
    case args of
      [] -> hPutStrLn stderr "No filename given"
      _ -> mapM_ putStrLn (process args)
