module Main where

import System.Directory
import System.Environment
import System.IO

main :: IO ()
main = do
    args <- getArgs

    let missing = if elem "-p" args
                 then True
                 else False

    let cleaned = filter (/= "-p") args

    case cleaned of
      [] -> hPutStrLn stderr "No directory given"
      _  -> mapM_ (createDirectoryIfMissing missing) cleaned
