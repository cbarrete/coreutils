module Main where

import System.Environment

main :: IO ()
main = do
    args <- getArgs
    case length args of
      0 -> interact (unlines . lines)
      _ -> mapM_ (\f -> readFile f >>= putStrLn) args
